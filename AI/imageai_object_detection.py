from imageai.Detection import VideoObjectDetection
import os
import cv2


def forFrame(frame_number, output_array, output_count):
    print("FOR FRAME ", frame_number)
    print("Output for each object : ", output_array)
    print("Output count for unique objects : ", output_count)
    print("------------END OF A FRAME --------------")


execution_path = os.getcwd()
model_path = os.path.join(execution_path, "models\\")

video = cv2.VideoCapture(0)

detector = VideoObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath(os.path.join(model_path, "yolo.h5"))
detector.loadModel()

custom_objects = detector.CustomObjects(person=True, bicycle=True, motorcycle=True, truck=True, stop_sign=True)

video_path = detector.detectCustomObjectsFromVideo(
    camera_input=video,
    output_file_path=os.path.join(model_path, "camera_detected_video"),
    per_frame_function=forFrame,
    frames_per_second=20,
    log_progress=True,
    minimum_percentage_probability=30
)

while True:
    success, frame = video.read()

    cv2.imshow("Camera", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break

print(video_path)
