#include <Ultrasonic.h>

float S1,S2,S3,S4,S5,S6,S7,S8,S9,S10;

int infra_frente = 22;
int infra_direita = 23;
int infra_esquerda = 24;
int infra_traseira = 25;

Ultrasonic ultra_frente(26,4); // (pino_trigger,pino_echo)
Ultrasonic ultra_direita(27,5); // (pino_trigger,pino_echo)
Ultrasonic ultra_esquerda(28,6); // (pino_trigger,pino_echo)
Ultrasonic ultra_traseira(29,7); // (pino_trigger,pino_echo)

int encoder_um = 2;
int rpm_um;
volatile byte pulsos_um;
unsigned long timeold_um;
unsigned int pulsos_por_volta_um = 20;
void contador_um()
{
  //Incrementa contador
  pulsos_um++;
}

int encoder_dois = 3;
int rpm_dois;
volatile byte pulsos_dois;
unsigned long timeold_dois;
unsigned int pulsos_por_volta_dois = 20;
void contador_dois()
{
  //Incrementa contador
  pulsos_dois++;
}

void setup() {
  Serial.begin(9600);
  
  pinMode(infra_frente, INPUT);
  pinMode(infra_direita, INPUT);
  pinMode(infra_esquerda, INPUT);
  pinMode(infra_traseira, INPUT);
  
  pinMode(encoder_um, INPUT);
  attachInterrupt(0, contador_um, RISING);
  pulsos_um = 0;
  rpm_um = 0;
  timeold_um = 0;
  pinMode(encoder_dois, INPUT);
  attachInterrupt(0, contador_dois, RISING);
  pulsos_dois = 0;
  rpm_dois = 0;
  timeold_dois = 0;
}

void loop() {
  long microsec_us_1 = ultra_frente.timing();
  if ( 100 > ultra_frente.convert(microsec_us_1, Ultrasonic::CM)){
  S1 = 0;
  } else{
  S1 = 1;
  }

  long microsec_us_2 = ultra_direita.timing();
  if ( 100 > ultra_frente.convert(microsec_us_2, Ultrasonic::CM)){
  S2 = 0;
  } else {
  S2 = 1;
  }

  long microsec_us_3 = ultra_esquerda.timing();
  if ( 100 > ultra_esquerda.convert(microsec_us_3, Ultrasonic::CM)){
  S3 = 0;
  } else {
  S3 = 1;
  }

  long microsec_us_4 = ultra_traseira.timing();
  if ( 100 > ultra_traseira.convert(microsec_us_4, Ultrasonic::CM)){
  S4 = 0;
  } else{
  S4 = 1;
  }

  if(digitalRead(infra_frente) == LOW){
    S5=1;
  }else{
    S5=0;
  }

  if(digitalRead(infra_direita) == LOW){
    S6=1;
  }else{
    S6=0;
  }

  if(digitalRead(infra_esquerda) == LOW){
    S7=1;
  }else{
    S7=0;
  }

  if(digitalRead(infra_traseira) == LOW){
    S8=1;
  }else{
    S8=0;
  }

  if (millis() - timeold_um >= 1000)
  {
    //Desabilita interrupcao durante o calculo
    detachInterrupt(0);
    rpm_um = (60 * 1000 / pulsos_por_volta_um ) / (millis() - timeold_um) * pulsos_um;
    timeold_um = millis();
    pulsos_um = 0;
    S9 = rpm_um;
    attachInterrupt(0, contador_um, RISING);
  }

  if (millis() - timeold_dois >= 1000)
  {
    //Desabilita interrupcao durante o calculo
    detachInterrupt(0);
    rpm_dois = (60 * 1000 / pulsos_por_volta_dois ) / (millis() - timeold_dois) * pulsos_dois;
    timeold_dois = millis();
    pulsos_dois = 0;
    S10 = rpm_dois;
    attachInterrupt(0, contador_dois, RISING);
  }

  Serial.print(",");
  Serial.print(S1);
  Serial.print(",");
  Serial.print(S2);
  Serial.print(",");
  Serial.print(S3);
  Serial.print(",");
  Serial.print(S4);
  Serial.print(",");
  Serial.print(S5);
  Serial.print(",");
  Serial.print(S6);
  Serial.print(",");
  Serial.print(S7);
  Serial.print(",");
  Serial.print(S8);
  Serial.print(",");
  Serial.print(S9, DEC);
  Serial.print(",");
  Serial.println(S10, DEC);
  //delay(200);
}
