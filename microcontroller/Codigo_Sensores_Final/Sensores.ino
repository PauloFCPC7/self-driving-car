void sensores() {
  if ( 100 < ultra_frente_direita.distance()){
  S1 = 0;  
  } else{
  S1 = 1;
  }

  if ( 100 < ultra_direita.distance()){
  S2 = 0;
  } else {
  S2 = 1;
  }

  if ( 100 < ultra_frente_esquerda.distance()){
  S3 = 0;
  } else {
  S3 = 1;
  }
  
  if ( 100 < ultra_esquerda.distance()){
  S4 = 0;
  } else{
  S4 = 1;
  }
  
  if(digitalRead(infra_frente) == LOW){
    S5=1;
  }else{
    S5=0;
  }
   
  if(digitalRead(infra_direita) == LOW){
    S6=1;
  }else{
    S6=0;
  }
  
  if(digitalRead(infra_esquerda) == LOW){
    S7=1;
  }else{
    S7=0;
  }
  
  if(digitalRead(infra_traseira) == LOW){
    S8=1;
  }else{
    S8=0;
  }

  if (millis() - timeold_um >= 1000)
  {
    //Desabilita interrupcao durante o calculo
    detachInterrupt(0);
    rpm_um = (60 * 1000 / pulsos_por_volta_um ) / (millis() - timeold_um) * pulsos_um;
    timeold_um = millis();
    pulsos_um = 0;
    S9 = rpm_um;
    attachInterrupt(0, contador_um, RISING);
  }

  if (millis() - timeold_dois >= 1000)
  {
    //Desabilita interrupcao durante o calculo
    detachInterrupt(0);
    rpm_dois = (60 * 1000 / pulsos_por_volta_dois ) / (millis() - timeold_dois) * pulsos_dois;
    timeold_dois = millis();
    pulsos_dois = 0;
    S10 = rpm_dois;
    attachInterrupt(0, contador_dois, RISING);
  }
  Serial.print(",");
  Serial.print(S1);
  Serial.print(",");
  Serial.print(S2);
  Serial.print(",");
  Serial.print(S3);
  Serial.print(",");
  Serial.print(S4);
  Serial.print(",");
  Serial.print(S5);
  Serial.print(",");
  Serial.print(S6);
  Serial.print(",");
  Serial.print(S7);
  Serial.print(",");
  Serial.print(S8);
  Serial.print(",");
  Serial.print(S9);
  Serial.print(",");
  Serial.println(S10);
}
