#include "Ultrasonic.h"

int S1,S2,S3,S4,S5,S6,S7,S8,S9,S10;

int infra_frente = 22;
int infra_direita = 23;
int infra_esquerda = 24;
int infra_traseira = 25;

HC_SR04 ultra_frente_direita(37,36); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_direita(39,38); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_frente_esquerda(33,32); //Configura os pinos sensor ultrassonico (Trigger,Echo)
HC_SR04 ultra_esquerda(35,34); //Configura os pinos sensor ultrassonico (Trigger,Echo)


int encoder_um = 2;
int rpm_um;
volatile byte pulsos_um;
unsigned long timeold_um;
unsigned int pulsos_por_volta_um = 20;
void contador_um()
{
  //Incrementa contador
  pulsos_um++;
}
int encoder_dois = 3;
int rpm_dois;
volatile byte pulsos_dois;
unsigned long timeold_dois;
unsigned int pulsos_por_volta_dois = 20;
void contador_dois()
{
  //Incrementa contador
  pulsos_dois++;
}

void setup() {
  Serial.begin(9600);
  pinMode(infra_frente, INPUT);
  pinMode(infra_direita, INPUT);
  pinMode(infra_esquerda, INPUT);
  pinMode(infra_traseira, INPUT);
  
  pinMode(encoder_um, INPUT);
  attachInterrupt(0, contador_um, RISING);
  pulsos_um = 0;
  rpm_um = 0;
  timeold_um = 0;
  pinMode(encoder_dois, INPUT);
  attachInterrupt(0, contador_dois, RISING);
  pulsos_dois = 0;
  rpm_dois = 0;
  timeold_dois = 0;
}

void loop() {
  sensores();
}
