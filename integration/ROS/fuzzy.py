#! C:/opt/ros/noetic/x64/python.exe
# coding: utf-8

import rospy
from std_msgs.msg import Int16
from std_msgs.msg import String

import cv2
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

rospy.init_node("car_fuzzy")
pub = rospy.Publisher("/car_vel_fuzzy", Int16, queue_size=10)
rate = rospy.Rate(3)  # Hz

objeto = ctrl.Antecedent(np.arange(0, 2, 1), 'objeto')
sensor_ultra = ctrl.Antecedent(np.arange(0, 2, 1), 'sensor_ultra')
velocidade = ctrl.Consequent(np.arange(0, 101, 1), 'velocidade')

sensor_ultra['baixo'] = fuzz.trimf(sensor_ultra.universe, [0, 0, 0.5])
sensor_ultra['alto'] = fuzz.trimf(sensor_ultra.universe, [0.5, 1, 1])

objeto['baixo'] = fuzz.trimf(objeto.universe, [0, 0, 1])
objeto['alto'] = fuzz.trimf(objeto.universe, [0, 1, 1])

velocidade['baixo'] = fuzz.trimf(velocidade.universe, [0, 0, 0])
velocidade['médio'] = fuzz.trapmf(velocidade.universe, [0, 1, 99, 100])
velocidade['alto'] = fuzz.trimf(velocidade.universe, [99, 100, 100])

# correct rules
# rule1 = ctrl.Rule(sensor_ultra['baixo'] & objeto['baixo'], velocidade['alto'])
# rule2 = ctrl.Rule(sensor_ultra['alto'] & objeto['baixo'], velocidade['médio'])
# rule3 = ctrl.Rule(sensor_ultra['baixo'] & objeto['alto'], velocidade['médio'])
# rule4 = ctrl.Rule(sensor_ultra['alto'] & objeto['alto'], velocidade['baixo'])

rule1 = ctrl.Rule(objeto['baixo'], velocidade['alto'])
rule2 = ctrl.Rule(objeto['alto'], velocidade['baixo'])


class FuzzySubscriber:
    def __init__(self):
        self.ultrasonic = None
        self.object = None

    def ultrasonic_callback(self, msg):
        # "Store" message received.
        self.ultrasonic = msg.data

        # Compute stuff.
        self.main_fuzzy()

    def object_callback(self, msg):
        # "Store" the message received.
        self.object = msg.data

        # Compute stuff.
        self.main_fuzzy()

    def main_fuzzy(self):
        rospy.loginfo(self.object)

        # velocidade_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4])
        velocidade_ctrl = ctrl.ControlSystem([rule1, rule2])
        velocidade_simulador = ctrl.ControlSystemSimulation(velocidade_ctrl)

        # velocidade_simulador.input['sensor_ultra'] = self.ultrasonic
        velocidade_simulador.input['objeto'] = self.object

        # Computando o resultado
        velocidade_simulador.compute()
        # print("Velocidade Fuzzy: ", round(velocidade_simulador.output['velocidade']))

        vel_fuzzy = Int16()
        vel_fuzzy.data = int(round(velocidade_simulador.output['velocidade']))
        pub.publish(vel_fuzzy)
        rate.sleep()


if __name__ == '__main__':
    fuzzy_sub = FuzzySubscriber()

    # rospy.Subscriber('/car_status_ultrasonic', Int16, fuzzy_sub.ultrasonic_callback)
    rospy.Subscriber('/car_status_object', Int16, fuzzy_sub.object_callback)

    rospy.spin()
